Fx.Scene.Add('ch1-r1b', {
    inMemory: true,
    map: 'Chapter1_hall_b',
    load: function(){
        Game.cameraMode = 'freecamera';
        if(!this.cached) {
            this.map = MAPS[this.map];
            this.map.prepareObjects()
        }
        SOUNDS.siren.stop();
        SOUNDS.siren.played = false;

        Game.gui.fade = 1;
        Game.gui.fadeTarget = 0;
        this.gui = function(){
            Game.gui.update();
        }

        this.update = function(animate){
            let {KeyCheck} = Fx.Input;
            BACKGROUNDS['back_parallax_2'].update();
            BACKGROUNDS['back_parallax_2'].x = 64 + this.cameraX / 8;
            BACKGROUNDS['back_parallax_2'].y =0;
            if(this.map)
                this.map.display();


            Context.globalCompositeOperation = "hard-light"
            Context
                .color("#444")
                .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            Context.globalCompositeOperation = "source-over";
            Game.controlCamera(animate);
            Game.particlesUpdate();
        }
    }
});