Fx.Scene.Add('main-menu', {
    load: function(){
        SOUNDS.chapter_1.play();
        let currentButton = 0;
        let buttons = []
        let targetZoom = 1;
        let targetFade = 0;
        let fade = 1;
        let vent = new Vent();
        vent.x = Canvas.width / 2 - 4;
        vent.y = Canvas.height - 25;
        this.zoom = 2;
        addButton('Start Game', function(){
            setTimeout(function(){Fx.Scene.Load('ch1-r0')}, 1000)
            targetZoom = 4;
            targetFade = 1;
        })
        addButton('Authors', function(){
            Fx.Scene.Load('authors-menu')
        })
        this.pressed = {'1': false, '0': false}
        this.gui = function(){
            Context.globalAlpha = fade;
            Context
                .color("#000")
                .fillRect(0,0,Canvas.width, Canvas.height)
            Context.globalAlpha = 1;
        }

        this.update = function(animate){
            let {upStep, downStep} = Game.controls;

            BACKGROUNDS['back_menu_1'].update();
            BACKGROUNDS['back_menu_1'].y = this.cameraY / 2;
            SPRITES['game_logo'].draw(Canvas.width / 2, Canvas.height * 0.4 + Math.sin(animate / 50) * 3);
            buttons[currentButton].select = true;

            if(upStep){
                currentButton++
            }
            if(downStep){
                currentButton--
            }

            currentButton = Math.max(0, Math.min(currentButton, buttons.length - 1))
            this.cameraY -= (this.cameraY - currentButton * 24) / 8;
            this.zoom -= (this.zoom - targetZoom) / 16;
            fade -= (fade - targetFade) / 8;
        }

        function addButton(title, behavior){
            let but = new Button().updateTitle(title);
            but.x = Canvas.width / 2 - but.rect.x / 2;
            but.y = Canvas.height / 1.5 + buttons.length * 24;
            but.behavior = behavior

            buttons.push(but);
        }
    }
});

