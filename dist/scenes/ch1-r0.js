Fx.Scene.Add('ch1-r0', {
    inMemory: true,
    map: 'Chapter1',

    load: function () {
        Game.gui.fade = 1;
        Game.gui.disable = true;
        Game.gui.fadeTarget = 0;
        Game.control = true;

        if (!this.cached) {
            this.map = MAPS[this.map];
            this.map.prepareObjects()
        }

        this.gui = function () {
            Game.gui.update();
        }

        setTimeout(function () {
            Game.gui.disable = false;
        }.bind(this), 1000)


        this.update = function (animate) {
            if (!SOUNDS.siren.played) {
                SOUNDS.siren.play();
            }
            SOUNDS.siren.played = true;
            let {KeyCheck} = Fx.Input;
            BACKGROUNDS['back_parallax'].update();
            BACKGROUNDS['back_parallax'].x = this.cameraX / 4;
            BACKGROUNDS['back_parallax'].y = this.cameraY / 4;
            if (this.map)
                this.map.display();


            Context.globalCompositeOperation = "overlay"
            Context.globalAlpha = 0.8;
            Context
                .color("#000")
                .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            Context.globalCompositeOperation = "source-over";
            Context.globalAlpha = 1;
            Game.controlCamera(animate);
            Game.particlesUpdate();
        }

        MAPS['Chapter1_hall'].setSolid('$solid');

    }
});