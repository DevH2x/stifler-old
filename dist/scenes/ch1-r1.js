Fx.Scene.Add('ch1-r1', {
    inMemory: true,
    map: 'Chapter1_hall',
    load: function(){

        if(!this.cached) {
            this.map = MAPS[this.map];
            this.map.prepareObjects()
        }

        Game.gui.fade = 1;
        Game.gui.fadeTarget = 0;
        this.gui = function(){
            Game.gui.update();
        }

        this.update = function(animate){
            if(!SOUNDS.siren.played)
                SOUNDS.siren.play();
            SOUNDS.siren.played = true;
            let {KeyCheck} = Fx.Input;
            BACKGROUNDS['back_parallax'].update();
            BACKGROUNDS['back_parallax'].x = this.cameraX / 8;
            BACKGROUNDS['back_parallax'].y = - 32;
            if(this.map)
                this.map.display();


            Context.globalCompositeOperation = "hard-light"
            Context
                .color("#222")
                .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            Context.globalCompositeOperation = "source-over";
            Game.controlCamera(animate);
            Game.particlesUpdate();
        }

        MAPS['Chapter1_hall'].setSolid('$solid');

    }
});