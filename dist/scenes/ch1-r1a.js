Fx.Scene.Add('ch1-r1a', {
    inMemory: true,
    map: 'Chapter1_hall_a',
    load: function(){

        if(!this.cached) {
            this.map = MAPS[this.map];
            this.map.prepareObjects()
        }
        SOUNDS.siren.stop();

        Game.gui.fade = 1;
        Game.gui.fadeTarget = 0;
        this.gui = function(){
            Game.gui.update();
        }

        this.update = function(animate){
            let {KeyCheck} = Fx.Input;
            BACKGROUNDS['back_parallax_2'].update();
            BACKGROUNDS['back_parallax_2'].x = 64 + this.cameraX / 8;
            BACKGROUNDS['back_parallax_2'].y =0;
            if(this.map)
                this.map.display();


            Context.globalCompositeOperation = "hard-light"
            Context
                .color("#111")
                .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            Context.globalCompositeOperation = "source-over";
            Game.controlCamera(animate);
            Game.particlesUpdate();
        }
    }
});