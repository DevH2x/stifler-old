Fx.Scene.Add('font-generator', {

    loadmap: 'SandBox',
    load: function () {
        this.map = MAPS[this.loadmap];
        this.map.prepareObjects()
        Fx.Canvas.BackgroundColor("#000")
        Game.gui.fade = 1;
        Game.gui.fadeTarget = 0;
        Game.player.keys = [];

        this.gui = function(){
            Game.gui.update();
        }

        this.update = function (animate) {

            if (this.map)
                this.map.display();

            // Context.globalCompositeOperation = "hard-light"
            // Context
            //     .color("#222")
            //     .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            // Context.globalCompositeOperation = "source-over";
            Game.controlCamera(animate);
            Game.particlesUpdate();

            Context.globalCompositeOperation = "hard-light"
            Context.globalAlpha = 0.4
            Context
                .color("#000")
                .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            Context.globalAlpha = 1
            Context.globalCompositeOperation = "source-over";
            Game.controlCamera(animate);
            Game.particlesUpdate();
        }

        this.Destroy = function() {
            delete this.playerPosition;
        }

    },
});