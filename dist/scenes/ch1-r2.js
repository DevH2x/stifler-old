Fx.Scene.Add('ch1-r2', {
    inMemory: true,
    map: 'Chapter1_hall_2',
    load: function(){
        Game.cameraMode = 'freecamera';
        if(!this.cached) {
            this.map = MAPS[this.map];
            this.map.prepareObjects()
        }
        SOUNDS.siren.stop();
        SOUNDS.siren.played = false;

        Game.gui.fade = 1;
        Game.gui.fadeTarget = 0;
        Fx.Canvas.BackgroundColor("#000")

        this.gui = function(){
            Game.gui.update();
        }

        this.update = function(animate){
            let {KeyCheck} = Fx.Input;
            if(this.map)
                this.map.display();


            Context.globalCompositeOperation = "hard-light"
            Context.globalAlpha = 0.4
            Context
                .color("#000")
                .fillRect(this.cameraX, this.cameraY, Canvas.width, Canvas.height)
            Context.globalAlpha = 1
            Context.globalCompositeOperation = "source-over";
            Game.controlCamera(animate);
            Game.particlesUpdate();

        }
    }
});