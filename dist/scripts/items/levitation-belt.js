class LevitationBelt extends Item {
    constructor() {
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('belt');
        this.color = "#5af";
        this.pickupsound = 'menu_select';
    }

    onSpawn(){
        super.onSpawn();
        this.spriteIndex.frame.current = this.properties.skin || 0;
        this.itemname = 'Levitation belt';
    }

    onPickup(){
        Game.player.belt = true;
    }
}