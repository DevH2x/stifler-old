class Item extends Fx.Element {
    constructor() {
        super();
        this.animate = 0;
    }

    onSpawn(){
        this.x += 8; this.y += 8;
        this.light = new PointLight();
        this.light.x = this.x - 8;
        this.light.y = this.y - 8;
        this.light.properties = {
            scale: 0.2,
            color: this.color || "#fff"
        }
    }

    update() {
        this.animate ++;
        let {plr} = this.scene;
        let {useButton} = Game.controls;
        let {x, y} = this;
        this.spriteIndex.origin.y = 4 + Math.sin(this.animate / 10) * 2;
        this.light.y = this.y - Math.sin(this.animate / 10) * 2;
        if (Fx.Math.Distance(x, y, plr.x, plr.y + 4) < 16) {
            Game.log('item: ' + this.itemname);
            if (useButton) {
                this.onPickup();
                SOUNDS[this.pickupsound || 'menu_select'].play();
                this.light.destroy();
                this.destroy();
                plr.useButton = false;
            }
        }
    }
}