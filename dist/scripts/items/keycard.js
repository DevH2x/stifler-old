class Keycard extends Item {
    constructor() {
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('keycard');
        this.color = "#5af";
        this.pickupsound = 'menu_select';
    }

    onSpawn(){
        super.onSpawn();
        this.spriteIndex.frame.current = this.properties.skin || 0;
        this.itemname = 'key ' + this.properties.keyname;
    }

    onPickup(){
        Game.player.keys.push(this.properties.keyname)
    }
}