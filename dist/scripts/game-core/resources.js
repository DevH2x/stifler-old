Fx.Resources = {
    Scripts: [
        './scenes/main-menu.js',
        './scenes/ch1-r0.js',
        './scenes/ch1-r1.js',
        './scenes/ch1-r1a.js',
        './scenes/ch1-r1b.js',
        './scenes/ch1-r2.js',
        './scenes/polygon-2.js',
        './scenes/fontgenerator.js',
        './scripts/entities/player.js',
        './scripts/entities/vent.js',
        './scripts/entities/elevator.js',
        './scripts/entities/trigger.js',
        './scripts/areas/triggerblock.js',
        './scripts/fx/alertlight.js',
        './scripts/fx/lightspot.js',
        './scripts/fx/pointlight.js',
        './scripts/fx/bullet.js',
        './scripts/entities/door.js',
        './scripts/areas/doorblock.js',
        './scripts/gui/buttons.js',
        './scripts/areas/explosionarea.js',
        './scripts/fx/explosion.js',
        './scripts/areas/area.js',
        './scripts/areas/ladder.js',
        './scripts/areas/blockseen.js',
        './scripts/areas/particles.js',
        './scripts/areas/spawner.js',
        './scripts/entities/bottle.js',
        './scripts/items/item.js',
        './scripts/items/keycard.js',
        './scripts/items/levitation-belt.js',
        './scripts/entities/turret.js',
        './scripts/entities/ventdoor.js',
        './scripts/entities/box.js',
        './scripts/entities/socket.js',
        './scripts/entities/mechbutton.js',
    ],
    Sprites: {
        plr_run: {
            file: './gfx/player-run.png',
            frameSize: vec2(16, 16),
            speed: 0.4,
            origin: vec2(8, 8)
        },
        plr_idle: {
            file: './gfx/player-idle.png',
            frameSize: vec2(16, 16),
            speed: 0.5,
            origin: vec2(8, 8)
        },
        plr_jump: {
            file: './gfx/player-jump.png',
            frameSize: vec2(16, 16),
            speed: 0.5,
            origin: vec2(8, 8)
        },
        plr_fall: {
            file: './gfx/player-fall.png',
            frameSize: vec2(16, 16),
            speed: 0.5,
            origin: vec2(8, 8)
        },
        plr_impact: {
            file: './gfx/player-impact.png',
            frameSize: vec2(16, 16),
            speed: 0.5,
            origin: vec2(8, 8)
        },
        plr_wakeup: {
            file: './gfx/player-wake-up.png',
            frameSize: vec2(16, 16),
            speed: 0.3,
            origin: vec2(8, 8)
        },
        vent: {
            file: './gfx/vent.png',
            origin: vec2(16, 16),
            speed: 0
        },
        elevator: {
            file: './gfx/elevator1.png',
            origin: vec2(0, 8),
            frameSize: vec2(48, 24),
            speed: 0
        },
        trigger: {
            file: './gfx/trigger01.png',
            origin: vec2(0, 0),
            frameSize: vec2(16, 16),
            speed: 0
        },
        alert_light: {
            file: './gfx/alertlight.png',
            origin: vec2(64, 128)
        },
        light: {
            file: './gfx/light.png'
        },
        light_long: {
            file: './gfx/lightspot_long.png'
        },
        door_unlocked: {
            file: './gfx/door-unlocked.png',
            frameSize: vec2(32, 48),
            speed: 0
        },
        door_locked: {
            file: './gfx/door-locked.png'
        },
        game_logo: {
            file: './gfx/spr-gamelogo.png',
            origin: vec2(64, 32)
        },
        pointlight: {
            file: './gfx/pointlight.png',
            origin: vec2(64, 64)
        },
        explosion: {
            file: './gfx/explosion.png',
            frameSize: vec2(64, 64),
            origin: vec2(32, 32),
            speed: 0.5
        },
        soldier: {
            file: './gfx/soldier.png',
            frameSize: vec2(64, 64),
            origin: vec2(32, 32),
            speed: 1
        },
        spark: {
            file: './gfx/spark.png',
            origin: vec2(4, 0)
        },
        dust: {
            file: './gfx/dust.png',
            origin: vec2(4, 4)
        },
        bottle: {
            file: './gfx/bottle.png',
            origin: vec2(4, 4),
            frameSize: vec2(8,8),
            speed: 0
        },
        keycard: {
            file: './gfx/keycard.png',
            origin: vec2(4, 4),
            frameSize: vec2(8,8),
            speed: 0
        },
        belt: {
            file: './gfx/levitation-belt.png',
            origin: vec2(4, 4),
            frameSize: vec2(8,8),
            speed: 0
        },
        turret_install: {
            file: './gfx/turret/turret-install.png',
            origin: vec2(0, 0),
            frameSize: vec2(16,16),
            speed: 0.3
        },
        turret_head: {
            file: './gfx/turret/turret-head.png',
            origin: vec2(8, 8)
        },
        turret_basic: {
            file: './gfx/turret/turret-basic.png',
            origin: vec2(0, 0)
        },
        vent_open: {
            file: './gfx/ventopen.png',
            origin: vec2(0, 0),
            frameSize: vec2(16,16),
            speed: 0
        },
        box: {
            file: './gfx/box.png',
            origin: vec2(8, 8),
            frameSize: vec2(16,16),
            speed: 0
        },
        invis: {
            file: './gfx/invis.png',
            origin: vec2(8, 8),
            frameSize: vec2(16,16),
            speed: 0.5
        },
        mechbutton: {
            file: './gfx/mechbutton.png',
            frameSize: vec2(16,16),
            speed: 0
        },

    },
    Fonts: {
        basic: {src: './fonts/gamefont_0.png', letter: vec2(16, 16)},
        italic: {src: './fonts/gamefont_italic.png', letter: vec2(16, 16)},
        alien: {src: './fonts/alien-font.png', letter: vec2(16, 16)},
    },
    Backgrounds: {
        back_parallax: {
            file: './gfx/back-parralax-1.png',
            repeatY: false,
            opacity: 0.5,
            fixed: true,
            margin: vec2(128,128)
        },
        back_parallax_2: {
            file: './gfx/back-parralax-2.png',
            repeatY: false,
            opacity: 0.5,
            fixed: true
        },
        back_menu_1: {
            file: './gfx/bg1.png'
        }
    },
    Maps: {
        Chapter1: {
            file: './maps/test.json',
            solid: '$solid',
        },
        Chapter1_hall: {
            file: './maps/ch1-r1.json',
            solid: '$solid',
        },
        Chapter1_hall_a: {
            file: './maps/ch1-r1a.json',
            solid: '$solid',
        },
        Chapter1_hall_b: {
            file: './maps/ch1-r1b.json',
            solid: '$solid',
        },
        Chapter1_hall_2: {
            file: './maps/ch1-r2.json',
            solid: '$solid',
        },
        SandBox: {
            file: './maps/test-polygon.json',
            solid: '$solid',
        },
        SandBox2: {
            file: './maps/test-polygon-2.json',
            solid: '$solid',
        }
    },
    Sounds: {
        siren: {
            src: ['./sounds/siren.ogg'],
            loop: true,
            volume: 1
        },
        granted: {
            src: ['./sounds/granted.ogg']
        },
        elevator_start: {
            src: ['./sounds/elevatorstart.ogg']
        },
        elevator_stop: {
            src: ['./sounds/elevatorstop.ogg']
        },
        elevator_loop: {
            src: ['./sounds/elevatorloop.ogg'],
            loop: true
        },
        plr_step: {
            src: ['./sounds/step.ogg']
        },
        plr_jump: {
            src: ['./sounds/jump.ogg']
        },
        plr_ground: {
            src: ['./sounds/ground.ogg']
        },
        door_open: {
            src: ['./sounds/dooropen.ogg'],
            volume: 0.3
        },
        door_close: {
            src: ['./sounds/doorclose.ogg'],
            volume: 0.3
        },
        menu_select: {
            src: ['./sounds/menu_select_1.ogg'],
            volume: 0.3
        },
        exp: {
            src: ['./sounds/explosion.ogg'],
            volume: 1
        },
        exp_out: {
            src: ['./sounds/explosion_out.ogg'],
            volume: 1
        },
        error: {
            src: ['./sounds/error.ogg'],
            volume: 1
        },
        unlock: {
            src: ['./sounds/unlock.ogg'],
            volume: 1
        },
        cankick: {
            src: ['./sounds/cankick.ogg'],
            volume: 1
        },
        belt_impact: {
            src: ['./sounds/beltimpact.ogg'],
            volume: 1
        },
        /*Music*/
        chapter_1: {
            src: ['./sounds/music/celestial_body_subterranean_ocean.ogg'],
            volume: 0.5
        },
    }
};