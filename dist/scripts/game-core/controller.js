let {ParticleSystem} = Fx;
Game = {
    gui: {
        fade: 0,
        fadeTarget: 0,
        disable: false,
        text: '',
        defaultTimer: 60 * 3,
        textTimer: 60 * 3,
        update: function () {
            Context.globalAlpha = this.fade;
            Context
                .color("#000")
                .fillRect(0, 0, Canvas.width, Canvas.height)
            Context.globalAlpha = 1;
            if (!this.disable)
                this.fade -= (this.fade - this.fadeTarget) / 16
            Fx.Font.Align = "cm";
            Fx.Font.DrawText('basic', Canvas.width / 2, 64, (this.text || '').toUpperCase(), 7);
            if (this.text !== '') {
                if (--this.textTimer < 1) {
                    this.textTimer = this.defaultTimer;
                    this.text = '';
                }
            }
        }
    },
    log: function (text, time) {
        this.gui.text = text;
        this.gui.textTimer = time || this.gui.defaultTimer;
    },
    shake: 0,
    controls: true,
    controlCamera: function (animate) {
        let {plr} = Fx.Scene.Current;
        let scene = Fx.Scene.Current;
        if (plr) {
            switch (this.cameraMode) {
                case 'segment':
                    scene.cameraX -= (scene.cameraX - Math.floor(plr.x / Canvas.width) * Canvas.width) / 8;
                    scene.cameraY -= (scene.cameraY - Math.floor(plr.y / Canvas.height) * Canvas.height) / 16;
                    break;
                case 'freecamera':
                    scene.cameraX -= (scene.cameraX - (plr.x - Canvas.width / 2 + plr.speed.x * 32)) / 8;
                    scene.cameraY -= (scene.cameraY - (plr.y - Canvas.height / 2 - 30)) / 8;
                    break;
            }

            scene.cameraX = Math.round(Math.max(0, Math.min(scene.cameraX + Math.round(Math.sin(animate) * this.shake), scene.map.width * 16 - Canvas.width)));
            scene.cameraY = Math.round(Math.max(0, Math.min(scene.cameraY + Math.round(Math.sin(-animate / 2) * this.shake), scene.map.height * 16 - Canvas.height)));
        }

        this.shake -= this.shake / 16;
    },
    cameraMode: 'freecamera',
    lang: {
        current: 'en',
        ru: {
            "keycard_need": "Нужен ключ #"
        },
        en: {
            "keycard_need": "You need # keycard"
        }
    },
    player: {
        keys: [],
        checkKey: function (key) {
            for (let i = 0; i < this.keys.length; i++) {
                if (this.keys[i] === key) {
                    return true;
                }
            }
            return false;
        },
        addKey(key) {
            this.keys.push(key);
        }
    },
    particlesUpdate: function(){
        for(let i in this.Particles){
            this.Particles[i].update();
        }
    },
    Particles:{
        dustmotes: new ParticleSystem({
            life: [60, 120],
            speed: [.01, .1],
            direction: [0, 360],
            color: "#fff",
            alpha: 0.5,
            scale: [.2, .2],
            gravity: 0.0,
            blend: 'source-over'
        }),
        explosion:new ParticleSystem({
            life: [60, 120],
            speed: [1, 10],
            direction: [0, 360],
            color: "#ff0",
            alpha: 1,
            scale: [.5, .5],
            gravity: 0.5,
            blend: 'screen'
        }),
        snow:new ParticleSystem({
            life: [60, 120],
            speed: [1, 10],
            direction: [85, 95],
            color: "#f00",
            alpha: 1,
            scale: [1, 1],
            gravity: 0.5,
            shape: 'sprite',
            sprite: 'spark',
            blend: 'screen'
        }),
        spark:new ParticleSystem({
            life: [60, 120],
            speed: [-4, 4],
            direction: [85, 95],
            angle: [90,90],
            color: "#f00",
            alpha: 1,
            scale: [.5, .5],
            gravity: 0.2,
            shape: 'sprite',
            sprite: 'spark',
            blend: 'source-over'
        }),
        dust: new ParticleSystem({
            life: [60, 120],
            speed: [.01, .1],
            direction: [0, 360],
            angle: [0, 360],
            color: "#fff",
            alpha: 0.5,
            scale: [1, 2],
            gravity: 0.0,
            shape: 'sprite',
            sprite: 'dust',
            blend: 'source-over'
        }),
        belt_impact: new ParticleSystem({
            life: [60, 120],
            speed: [-2,2],
            direction: [0, 0],
            angle: [0,0],
            color: "#fff",
            alpha: 0.5,
            scale: [2, 4],
            gravity: 0,
            shape: 'sprite',
            sprite: 'spark',
            blend: 'source-over'
        }),
    },
    controls:{
        enabled: true,
        rightButton: false,
        leftButton: false,
        jumpButton: false,
        useButton: false,
        upButton: false,
        downButton: false,
        pressed:[]
    }
}

Fx.SceneBefore = function(){
    let {GPads, KeyCheck, GamepadCheckButton} = Fx.Input;
    let axeRight = GPads[0].axes[0] > 0.5;
    let axeLeft = GPads[0].axes[0] < -0.5;
    let axeUp = GPads[0].axes[1] > 0.5;
    let axeDown = GPads[0].axes[1] < -0.5;
    Game.controls.rightButton = KeyCheck(39) || axeRight;
    Game.controls.leftButton = KeyCheck(37) || axeLeft;
    Game.controls.jumpButton = KeyCheck(" ") || (Game.controls.pressed[2]? false: GamepadCheckButton(0,2));
    Game.controls.useButton = KeyCheck("X") || (Game.controls.pressed[1]? false: GamepadCheckButton(0,1));
    Game.controls.upButton = KeyCheck(40) || axeUp;
    Game.controls.downButton = KeyCheck(38) || axeDown;

    /*One step buttons*/
    Game.controls.upStep = KeyCheck(40) || (Game.controls.pressed[9]? false: axeUp);
    Game.controls.downStep = KeyCheck(38) || (Game.controls.pressed[10]? false: axeDown);
    Game.controls.leftStep = KeyCheck(37) || (Game.controls.pressed[11]? false: axeLeft);
    Game.controls.rightStep = KeyCheck(39) || (Game.controls.pressed[12]? false: axeRight);
}

Fx.SceneAfter = function(){
    let {GPads, KeyReset, GamepadCheckButton} = Fx.Input;
    let axeRight = GPads[0].axes[0] > 0.5;
    let axeLeft = GPads[0].axes[0] < -0.5;
    let axeUp = GPads[0].axes[1] > 0.5;
    let axeDown = GPads[0].axes[1] < -0.5;
    Game.controls.pressed[2] = GamepadCheckButton(0,2);
    Game.controls.pressed[1] = GamepadCheckButton(0,1);
    Game.controls.pressed[9] = axeUp;
    Game.controls.pressed[10] = axeDown;
    Game.controls.pressed[11] = axeLeft;
    Game.controls.pressed[12] = axeRight;
    KeyReset(" ")
    KeyReset("X")
}
