window.addEventListener('load', function () {
    let aspect = 16/9
    var w = 320*1.5, h = (320*1.5) / aspect;
    Fx.Config = {
        width: w,
        height: h,
        bgColor: "#222",
        imageSmoothingEnabled: false
    };




    Fx.Core.PrepareCanvas('#canvas');
    //Fx.Canvas.Fit(screen.width, screen.width / aspect);
    Fx.Canvas.Fit(w * 2, h * 2);

    Fx.OnLoad = function () {
        Fx.Scene.Load('main-menu');
    };
})