class Button extends Fx.Element {

    constructor(){
        super()
        this.title = "Sample Text";
        this.select = false;
        this.pressed = {'1': false, '2': false}
    }
    update(){
        let {useButton} = Game.controls;
        Context.shadowColor = "#fff"
        Context.shadowBlur = this.select * 8;
        Fx.Font.Align = "lm";
        Fx.Font.DrawText('basic', this.x, this.y, this.title.toUpperCase(), 7);
        Context.shadowBlur = 0;
        if(useButton){
            SOUNDS.menu_select.play();
            this.behavior();
        }

        this.select = false;

    }

    updateTitle(title){
        Context._font('14px Game Font')
        this.title = title;
        this.rect = vec2(Context.measureText(title.toUpperCase()).width, 14);

        return this;
    }
}