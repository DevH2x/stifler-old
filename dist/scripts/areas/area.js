class Area extends Fx.Element{
    constructor(){
        super();
    }

    onSpawn(){
        this.y += 16;
    }

    playerTouched(){
        let {plr} = this.scene;
        if(plr){
            if(plr.x > this.x && plr.x < this.x + this.width && plr.y > this.y && plr.y < this.y + this.height){
                return true;
            }
        }
        return false;
    }
}