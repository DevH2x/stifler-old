class BlockSeen extends Area{

    constructor(){
        super();
        this.alpha = 1;
        this.seen = false;
    }
    update(){
        if(this.playerTouched()){
            this.seen = true;
        }

        if((this.alpha -= 0.1*this.seen) < 0)
            this.destroy();

        Context.globalAlpha = Math.max(0, this.alpha);
        Context.shadowBlur = 16;
        Context.shadowColor = "#000";
        Context.color("#000").fillRect(this.x, this.y, this.width, this.height);
        Context.globalAlpha = 1;
        Context.shadowBlur = 0;
    }
}