class TriggerBlock extends Fx.Element{
    constructor(){
        super();
        this.properties = {
            script: ''
        };

        this.spawned = false;

    }

    update(){
        if(!this.spawned){
            this.rect = vec2(this.width, this.height);
            this.y += 16;
            this.spawned = true;
        }

        let {plr} = this.scene;
        if(plr){
            if(plr.x > this.x && plr.x < this.x + this.width && plr.y > this.y && plr.y < this.y + this.height){
                eval(this.properties.script);
                this.destroy();
            }
        }
    }
}