class Spawner extends Area{
    onSpawn(){
        let{x, y, width, height} = this;
        for(let i = 0; i < this.properties.count; i++){
            let o = eval('new ' + this.properties.type)
            o.x = x + Math.random() * width
            o.y = y + Math.random() * height
        }
    }
}