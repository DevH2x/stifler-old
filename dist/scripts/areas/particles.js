class Particles extends Fx.Element{
    constructor(){
        super();
        this.properties = {
            particle_name: '',
            interval: 16
        }
    }

    onSpawn(){
        let {particles,x ,y, width, height} = this;
        this.particles = Game.Particles[this.properties.particle_name] || null;
        this.interval = setInterval(function(){
            if(Fx.Scene.Current !== this.scene) return;
            this.particles.stream(x + width * Math.random(), y + height * Math.random());
        }.bind(this), this.properties.interval)
    }

    destroy(){
        super.destroy()
    }
}