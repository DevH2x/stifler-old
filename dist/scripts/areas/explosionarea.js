class ExplosionArea extends Fx.Element {
    constructor(){
        super()
        this.properties = {
            explosion_count: 0,
            interval: 1000
        }
    }

    explode(){
        new Explode(this.x + this.width * Math.random(), this.y + this.height * Math.random());
        if(this.properties.explosion_count > 0) {
            setTimeout(function () {
                this.explode();
            }.bind(this), this.properties.interval / 2 + Math.random() * this.properties.interval / 2)
            this.properties.explosion_count--;
        }
    }
}