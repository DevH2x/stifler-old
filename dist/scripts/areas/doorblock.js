class DoorBlock extends Fx.Element{
    constructor(){
        super();
        this.properties = {
            activator: null,
            inverted_signal: false
        };

        this.spawned = false;

    }

    update(){
        if(!this.spawned){
            this.rect = vec2(this.width, this.height);
            this.y += 16;
            Fx.ObjectController.AddSolidElement(this);
            this.spawned = true;
        }

        Context
            .color("#000")
            .fillRect(this.x, this.y, this.rect.x, this.rect.y);
        if(this.properties.activator) {
            this.rect.y -= (this.rect.y - this.height * (this.properties.inverted_signal ? this.scene[this.properties.activator].properties.active : !this.scene[this.properties.activator].properties.active)) / 8;
        }
    }

    elevatorSoundStart(){
        if(!this.started){
            SOUNDS.elevator_start.play();
            setTimeout(()=>{
                SOUNDS.elevator_loop.play();
            }, 700)
            this.started = true;
        }
    }

    elevatorSoundEnd(){
        if(this.started){
            SOUNDS.elevator_stop.play();
        }
        this.started = false;
        SOUNDS.elevator_loop.stop();
    }
}