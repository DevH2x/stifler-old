class Vent extends Fx.Element{
    constructor(){
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('vent');
        this.angleSpeed = 5 + Math.random() * 10;
    }

    update(){
        this.spriteIndex.angle += this.angleSpeed;
    }
}