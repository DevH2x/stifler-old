class Door extends Fx.Element{
    constructor(){
        super();
        this.sprites = [
            Fx.ResourceManager.GetSprite('door_unlocked'),
            Fx.ResourceManager.GetSprite('door_locked')
        ]
        this.spriteIndex = this.sprites[0]
        this.properties = {
            locked: false,
            scene: null,
            keycard: ''
        }

        this.spawned = false;
        this.started = false;
    }
    onSpawn(){

    }
    update(){
        let {plr} = Fx.Scene.Current;
        let {useButton} = Game.controls;
        let doorSpeed = 0.2;
        this.spriteIndex = this.sprites[this.properties.locked?1:0];
        if(plr){
            let x = this.x + 8;
            let y = this.y + 8;
            if(!this.properties.locked) {
                if (Fx.Math.Distance(x+16, y+32, plr.x, plr.y) < 32) {
                    if (this.spriteIndex.frame.current < 6) {
                        this.spriteIndex.frame.current += doorSpeed;
                        this.doorSoundStart()
                    } else {
                        this.spriteIndex.frame.current = 6;
                        if(useButton && this.properties.scene){
                            Game.gui.fadeTarget = 1;
                            setTimeout(function(){
                                Fx.Scene.Load(this.properties.scene);
                            }.bind(this), 1000)

                        }
                    }
                } else {
                    if (this.spriteIndex.frame.current > 1) {
                        this.spriteIndex.frame.current -= doorSpeed;
                        this.doorSoundEnd()
                    } else {
                        this.spriteIndex.frame.current = 0;
                    }
                }
            }else{
                this.spriteIndex.frame.current = 0;
                if(Fx.Math.Distance(x+16, y+32, plr.x, plr.y) < 32 && this.properties.keycard !== ''){
                    if(useButton){
                        if(Game.player.checkKey(this.properties.keycard)){
                            this.properties.locked = false;
                            SOUNDS['unlock'].play();
                        }else {
                            Game.log(Game.lang[Game.lang.current]['keycard_need'].replace("#", this.properties.keycard));
                            SOUNDS['error'].play();
                        }
                    }
                }
            }
        }
    }
    doorSoundStart(){
        if(!this.started){
            SOUNDS.door_close.stop();
            SOUNDS.door_open.play();
            this.started = true;
        }
    }

    doorSoundEnd(){
        if(this.started){
            SOUNDS.door_open.stop();
            SOUNDS.door_close.play();
        }
        this.started = false;
    }
}