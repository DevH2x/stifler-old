class Player extends Fx.Element {

    constructor() {
        super();
        this.gravity = 0.25;
        this.friciton = 0.9;
        this.touch = false;
        this.secondJump = false;
        this.ladder = false;
        this.sprites = {
            'run': Fx.ResourceManager.GetSprite('plr_run'),
            'idle': Fx.ResourceManager.GetSprite('plr_idle'),
            'jump': Fx.ResourceManager.GetSprite('plr_jump'),
            'fall': Fx.ResourceManager.GetSprite('plr_fall'),
            'impact': Fx.ResourceManager.GetSprite('plr_impact'),
            'wakeup': Fx.ResourceManager.GetSprite('plr_wakeup')
        }
        this.spriteIndex = this.sprites['idle'];

        this.spawned = false;
        this.useButton = false;
        this.pressed = {'1': false, '2': false, '3': false, '4': false}
        this.impact = 1;
        this.targetSocket = false;
        this.socketIndex = 0;
    }
    onSpawn(){
        this.x += 8;
        this.y += 8;
        Fx.Scene.Current.cameraX = Math.floor(this.x / Canvas.width) * Canvas.width
        Fx.Scene.Current.cameraY = Math.floor(this.y / Canvas.height) * Canvas.height;

        if(this.scene.playerPosition) {
            this.x = this.scene.playerPosition.x
            this.y = this.scene.playerPosition.y - 2
            Fx.Scene.Current.cameraX = Math.floor(this.x / Canvas.width) * Canvas.width
            Fx.Scene.Current.cameraY = Math.floor(this.y / Canvas.height) * Canvas.height;
        }

        if(this.properties.sleep){
            this.spriteIndex = this.sprites['wakeup'];
            this.spriteIndex.frame.speed = 0;
            this.spriteIndex.frame.current = 0;
            Game.controls.enabled = false;

            setTimeout(function(){
                this.spriteIndex.frame.speed = 0.2;
            }.bind(this), 4000)
        }
    }
    update() {
        if(Game.controls.enabled && !this.targetSocket) {
            this.speed.y += this.gravity * !this.ladder;
            this.controls();
        }
        this.animation();
        if(!this.targetSocket)
            this.collision();

        if(this.targetSocket){
            let target = this.scene[this.targetSocket[this.socketIndex]]
            this.speed.y =
                this.speed.x = 0;
            this.x -= (this.x - target.x) / 16;
            this.y -= (this.y - target.y) / 16;
            this.spriteIndex = SPRITES['invis'];
            Game.Particles['dustmotes'].stream(this.x, this.y)
            if(Fx.Math.Distance(this.x, this.y, target.x, target.y) < 1){
                if(this.targetSocket[this.socketIndex + 1] != null){
                    this.socketIndex += 1
                }else {
                    this.targetSocket = null;
                    this.spriteIndex = this.sprites['fall'];
                    this.socketIndex = 0;
                }
            }
        }

        this.scene.playerPosition = vec2(this.x, this.y);
        this.ladder = false;
    }

    collision() {
        let {x, y, speed, insolid} = this;
        let {map} = Fx.Scene.Current;
        this.touch = map.isSolid(x, y + 9) || map.isSolid(x + 7, y + 9) || map.isSolid(x - 7, y + 9) || this.insolid(0, 9) || this.insolid(7, 9) || this.insolid(-7, 9);
        if(typeof this.touch === 'object'){
            this.touch.touched = true;
        }
        /*Vertical*/
        let _speed = Math.abs(Math.ceil(speed.y));
        let _dir = Math.sign(speed.y);
        for (let i = 0; i < _speed; i++) {
            let collide = map.isSolid(x, y + 8 * _dir + i * _dir) ||
                map.isSolid(x + 7, y + 8 * _dir + i * _dir) ||
                map.isSolid(x - 7, y + 8 * _dir + i * _dir) ||
                this.insolid(0, 8 * _dir + i * _dir) ||
                this.insolid(7, 8 * _dir + i * _dir) ||
                this.insolid(-7, 8 * _dir + i * _dir);
            if (collide) {
                if(typeof collide === 'object'){
                    this.y = collide.y + collide.rect.y * (speed.y < 0) - 8;
                }else {
                    this.y += i * _dir;
                }
                if(this.speed.y > 2){
                    SOUNDS.plr_ground.play();
                    this.impact = 0
                    this.secondJump = false;
                }
                this.speed.y = 0;
                break;
            }
        }

        /*Horizontal*/
        _speed = Math.abs(Math.round(speed.x))+1;
        _dir = Math.sign(speed.x);
        for (let i = 0; i < _speed; i++) {
            if (
                map.isSolid(x + 9 * _dir + i * _dir, y) ||
                map.isSolid(x + 9 * _dir + i * _dir, y + 7) ||
                map.isSolid(x + 9 * _dir + i * _dir, y - 7) ||
                this.insolid(9 * _dir + i * _dir, 7) ||
                this.insolid(9 * _dir + i * _dir, -7) ||
                this.insolid(9 * _dir + i * _dir, 0)) {
                this.x += i * _dir;
                this.speed.x = 0;
                break;
            }
        }
    }

    animation() {
        let {speed, touch, sprites, hint} = this;
        this.impact -= (this.impact - 1) / 16;

        if(this.properties.sleep){
            let currentFrame = Math.round(this.spriteIndex.frame.current);
            if (currentFrame == 29){
                Game.controls.enabled = true;
                this.properties.sleep = false;
            }
            return;
        }
        if (touch) {
            let currentFrame = Math.round(this.spriteIndex.frame.current);
            let nextSprite = sprites[Math.abs(speed.x) > 0.1 ? "run" : "idle"]
            if(this.spriteIndex == sprites["fall"]){
                this.spriteIndex = sprites['impact'];
                this.spriteIndex.frame.current = 0;
            }
            if(this.spriteIndex == sprites["impact"]){
                this.spriteIndex.frame.speed = 0.3;
                if(currentFrame == (Math.abs(speed.x) > 0.1 ?11:5)){
                    this.spriteIndex = nextSprite;
                    this.spriteIndex.frame.current = 0;
                }

                if(currentFrame > 5 && Math.abs(speed.x) < 0.01){
                    this.spriteIndex = nextSprite;
                }
            }else{
                this.spriteIndex = nextSprite;
            }
            if(Math.abs(speed.x) > 0.1){
                if(currentFrame == 7 ||
                    currentFrame == 15){
                    if(!SOUNDS.plr_step.played)
                        SOUNDS.plr_step.play();
                    SOUNDS.plr_step.played = true;
                }else{
                    SOUNDS.plr_step.played = false;
                }
            }
        } else {
            this.spriteIndex = sprites[speed.y > 0 ? "fall" : "jump"]
            this.spriteIndex.frame.current = 0;
            this.spriteIndex.frame.speed = 0;
        }
        if(hint){
            this.touch = false;
        }
    }

    controls() {
        let {map} = Fx.Scene.Current;
        let {KeyCheck, KeyReset, GPads, GamepadCheckButton} = Fx.Input;
        let {touch} = this;
        let {rightButton, leftButton, jumpButton, upButton, downButton} = Game.controls;

        this.speed.x += (rightButton - leftButton) * 0.5 * this.impact;
        if(this.ladder) {
            let path = (upButton - downButton) * 0.4;
            this.speed.y = path * (!map.isSolid(this.x, this.y + 8 * Math.sign(path) + path));
        }
        this.speed.x -= this.speed.x / 4;
        if (rightButton) this.spriteIndex.scale.x = 1;
        if (leftButton) this.spriteIndex.scale.x = -1;
        if (jumpButton) {
            if(this.secondJump) {
                this.speed.y = -4;
                SOUNDS.belt_impact.play();
                Game.Particles['belt_impact'].burst(this.x, this.y + 8, 8);
                this.secondJump = false;
            }

            if(touch) {
                this.y -= 3;
                this.speed.y = -4;
                SOUNDS.plr_jump.play();
                this.secondJump = Game.player.belt;
            }
        }

    }
}