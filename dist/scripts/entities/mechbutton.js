class MechButton extends Fx.Element{
    constructor(){
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('mechbutton');
        this.properties = {
            active: false
        }
    }

    update(){
        let{x, y} = this;
        let {Objects} = Fx.Scene.Current;
        let {mouseX, mouseY} = Fx.Input;
        this.properties.active = false;

        Objects.forEach(function(e){
            if(Fx.Math.Distance(x + 8, y + 8, mouseX, mouseY) < 16){
                this.properties.active = true;
            }
            this.spriteIndex.frame.current = this.properties.active;
        }.bind(this))
    }
}