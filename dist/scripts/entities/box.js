class Box extends Fx.Element {
    constructor() {
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('box');
        this.hint = false;
        this.animate = 0;
    }


    update() {
        let {plr} = this.scene;
        let {useButton} = Game.controls;
        let {x, y} = this;
        this.speed.y += 0.2;
        if (Fx.Math.Distance(x, y, plr.x, plr.y + 4) < 10) {
            if (useButton) {
                plr.hint = !plr.hint;
                this.hint = plr.hint;
            }
        }

        if(this.hint){
            this.animate += plr.speed.x;
            this.x = plr.x - Math.round(plr.speed.x);
            this.y = plr.y + Math.abs(Math.sin(this.animate / 10) * 2);
        }
        this.collision()
    }

    collision() {
        let {x, y, speed, insolid} = this;
        let {map} = Fx.Scene.Current;
        this.touch = map.isSolid(x, y + 3);

        /*Vertical*/
        let _speed = Math.abs(Math.ceil(speed.y));
        let _dir = Math.sign(speed.y);
        for (let i = 0; i < _speed; i++) {
            if (map.isSolid(x, y + 8 * _dir + i * _dir) ||
                map.isSolid(x + 7, y + 8 * _dir + i * _dir) ||
                map.isSolid(x - 7, y + 8 * _dir + i * _dir)) {
                this.y += i * _dir;
                this.speed.y = 0;

                break;
            }
        }

        /*Horizontal*/
        _speed = Math.abs(Math.round(speed.x)) + 1;
        _dir = Math.sign(speed.x);
        for (let i = 0; i < _speed; i++) {
            if (
                map.isSolid(x + 9 * _dir + i * _dir, y) ||
                map.isSolid(x + 9 * _dir + i * _dir, y + 1) ||
                map.isSolid(x + 9 * _dir + i * _dir, y - 1)) {
                this.x += i * _dir;
                this.speed.x = 0;
                break;
            }
        }
    }
}