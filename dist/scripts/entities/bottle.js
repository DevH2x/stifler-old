class Bottle extends Fx.Element {
    constructor() {
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('bottle');
        this.spriteIndex.frame.current = Math.round(Math.random() * 3);
    }


    update() {
        let {plr} = this.scene;
        let {useButton} = Game.controls;
        let {x, y} = this;
        this.speed.y += 0.2;
        if (Fx.Math.Distance(x, y, plr.x, plr.y + 4) < 4) {
            this.speed.x += plr.speed.x * 0.1;
            this.speed.y += -Math.abs(plr.speed.x) * 0.1;
            if (Math.abs(plr.speed.x) > 1 && useButton) {
                this.speed.x += plr.speed.x * 2;
                this.speed.y += -Math.abs(plr.speed.x) * 2;

            }
        }
        if (!this.touch) {
            this.spriteIndex.angle += this.speed.x * 10;
        } else {
            this.spriteIndex.angle = 0;
            this.speed.x *= 0.90;
        }
        this.collision()
    }

    collision() {
        let {x, y, speed, insolid} = this;
        let {map} = Fx.Scene.Current;
        this.touch = map.isSolid(x, y + 3);

        /*Vertical*/
        let _speed = Math.abs(Math.ceil(speed.y));
        let _dir = Math.sign(speed.y);
        for (let i = 0; i < _speed; i++) {
            if (map.isSolid(x, y + 2 * _dir + i * _dir) ||
                map.isSolid(x + 7, y + 2 * _dir + i * _dir) ||
                map.isSolid(x - 7, y + 2 * _dir + i * _dir)) {
                this.y += i * _dir;
                if(this.speed.y > 2){
                    if(this.incamera()){SOUNDS['cankick'].play()};
                }
                this.speed.y = 0;

                break;
            }
        }

        /*Horizontal*/
        _speed = Math.abs(Math.round(speed.x)) + 1;
        _dir = Math.sign(speed.x);
        for (let i = 0; i < _speed; i++) {
            if (
                map.isSolid(x + 9 * _dir + i * _dir, y) ||
                map.isSolid(x + 9 * _dir + i * _dir, y + 1) ||
                map.isSolid(x + 9 * _dir + i * _dir, y - 1)) {
                this.x += i * _dir;
                this.speed.x = 0;
                break;
            }
        }
    }
}