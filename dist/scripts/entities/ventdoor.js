class VentDoor extends Fx.Element{
    constructor(){
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('vent_open')
        this.properties = {
            locked: false,
            scene: null,
            keycard: ''
        }

        this.spawned = false;
        this.started = false;
    }
    onSpawn(){

    }
    update(){
        let {plr} = Fx.Scene.Current;
        let {useButton} = Game.controls;
        let doorSpeed = 0.2;
        if(plr){
            let x = this.x + 8;
            let y = this.y + 8;
            this.spriteIndex.frame.current = Fx.Math.Distance(x, y, plr.x, plr.y) < 10;
            if(Fx.Math.Distance(x, y, plr.x, plr.y) < 10){
                if(useButton){
                    Game.gui.fadeTarget = 1;
                    setTimeout(function(){
                        Fx.Scene.Load(this.properties.scene);
                    }.bind(this), 1000);
                }
            }
        }
    }
    doorSoundStart(){
        if(!this.started){
            SOUNDS.door_close.stop();
            SOUNDS.door_open.play();
            this.started = true;
        }
    }

    doorSoundEnd(){
        if(this.started){
            SOUNDS.door_open.stop();
            SOUNDS.door_close.play();
        }
        this.started = false;
    }
}