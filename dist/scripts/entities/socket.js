class Socket extends Fx.Element {
    constructor() {
        super();
        this.animate = 0;
        this.properties = {};
    }

    onSpawn(){
        this.x += 8
        this.y += 8
        this.target = this.scene[this.properties.target]
    }


    update() {
        let {plr} = this.scene;
        let {useButton} = Game.controls;
        let {x, y, target} = this;
        if (Fx.Math.Distance(x, y, plr.x, plr.y) < 16) {
            if (useButton && !this.properties.repeater && !plr.targetSocket) {
                plr.x = this.x;
                plr.y = this.y;
               plr.targetSocket = this.properties.target.split(',');
            }
        }
    }
}