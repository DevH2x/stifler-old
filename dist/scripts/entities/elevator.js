class Elevator extends Fx.Element {
    constructor() {
        super();
        this.rect = vec2(48, 16);
        this.spriteIndex = Fx.ResourceManager.GetSprite('elevator');
        this.properties = {
            activator: null,
            path: 0,
            stay_down: false,
            time: 1,
            time_start: 0,
            speed: 0,
            attach_activator: false
        };
        this.attach = {
            object: null,
            position: null
        }
        this.spawned = false;
        this.touched = false;
        this.started = false;
        this.startPoint = vec2(this.x, this.y);
        Fx.ObjectController.AddSolidElement(this);
    }

    onSpawn(){
        let {path, direction, time, activator, speed, time_start} = this.properties;
        this.properties.path = this.properties.path * 16;
        this.properties.time = this.properties.time * 60;
        this.properties.speed = (this.properties.path / (this.properties.time));
        this.properties.time_start = this.properties.time
        this.startPoint = vec2(this.x, this.y);
        if (this.properties.attach_activator) {
            let a = Fx.Scene.Current[activator];
            a.properties.active = this.properties.stay_down || false;
            this.attach.object = a;
            this.attach.position = vec2(a.x - this.x, a.y - this.y);
        }
        this.y = this.startPoint.y + this.properties.path * this.properties.stay_down;
    }

    update() {
        let {path, direction, time, activator, speed, time_start} = this.properties;
        let {plr} = Fx.Scene.Current;
        let activator_state = Fx.Scene.Current[activator].properties.active || false;
        if (!this.spawned) {

            this.spawned = true;
        }

        if (this.attach.object) {
            this.attach.object.x = this.x + this.attach.position.x;
            this.attach.object.y = this.y + this.attach.position.y;
        }

        if (activator_state) {
            if (this.y < this.startPoint.y + path) {
                this.elevatorSoundStart()
                this.y += speed;
                if (this.touched) plr.y += speed;
                this.spriteIndex.frame.speed = 0.5;
            } else {
                this.y = this.startPoint.y + path;
                this.spriteIndex.frame.speed = 0;
                this.elevatorSoundEnd()
            }
        } else {
            if (this.y > this.startPoint.y) {
                this.elevatorSoundStart()
                this.y -= speed;
                this.spriteIndex.frame.speed = 0.5;
            } else {
                this.elevatorSoundEnd()
                this.y = this.startPoint.y
                this.spriteIndex.frame.speed = 0;
            }
        }
        this.touched = false;
    }

    elevatorSoundStart() {
        if (!this.started) {
            Game.Particles['spark'].burst(this.x, this.y, 16);
            Game.Particles['spark'].burst(this.x + this.rect.x, this.y, 16);
            SOUNDS.elevator_start.play();
            setTimeout(() => {
                SOUNDS.elevator_loop.play();
            }, 700)
            this.started = true;
        }
    }

    elevatorSoundEnd() {
        if (this.started) {
            SOUNDS.elevator_stop.play();
            Game.Particles['dust'].burst(this.x, this.y + this.rect.y, 16);
            Game.Particles['dust'].burst(this.x + this.rect.x, this.y + this.rect.y, 16);
        }
        this.started = false;
        SOUNDS.elevator_loop.stop();
    }
}