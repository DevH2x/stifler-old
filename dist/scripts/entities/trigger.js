class Trigger extends Fx.Element{
    constructor(){
        super();
        this.spriteIndex = Fx.ResourceManager.GetSprite('trigger');
        this.properties = {
            active: false
        }
    }

    update(){
        let {plr} = Fx.Scene.Current;
        let {useButton} = Game.controls;

        if(plr){
            let x = this.x + 8;
            let y = this.y + 8;

            if(Fx.Math.Distance(x, y, plr.x, plr.y) < 16){
                if(useButton){
                    this.properties.active = !this.properties.active;
                    SOUNDS.granted.play();
                    plr.useButton = false;
                }
            }
            this.spriteIndex.frame.current = this.properties.active;
        }
    }
}