class Turret extends Fx.Element {
    constructor() {
        super();
        this.segments = {
            install: Fx.ResourceManager.GetSprite('turret_install'),
            head: Fx.ResourceManager.GetSprite('turret_head'),
            body: Fx.ResourceManager.GetSprite('turret_basic')
        }
        this.properties = {
            activator: ''
        }
        this.installed = true;
        this.timer = 10;
        this.animate = 0;
    }

    update() {
        let {activator} = this.properties;
        let active = activator !== '' ? this.scene[activator].properties.active : true;
        this.animate++
        if (active) {
            if (!this.installed) {
                this.segments['install'].frame.speed = 0.3;
                this.segments['install'].draw(this.x, this.y)
                if (Math.round(this.segments['install'].frame.current) === 9) {
                    this.installed = true;
                    this.segments['install'].frame.speed = -0.3;
                }
            } else {
                let {plr} = this.scene;
                let {x, y} = this;
                let alert = !this.castPlayer() && !plr.hint;
                this.segments['body'].draw(this.x, this.y);
                this.segments['head'].draw(this.x + 8, this.y + 12);
                this.segments['head'].angle = !alert * (90 + Math.sin(this.animate / 100) * 90) + alert * (Math.atan2((y + 12) - plr.y, (x + 8) - plr.x) * 180 / Math.PI + 180);
                if (alert) {
                    if (--this.timer < 1) {
                        let dist = Fx.Math.Distance(plr.x, 0, x, 0) / 4 * plr.speed.x
                        let dir = Math.atan2((y + 12) - plr.y - plr.speed.y, (x + 8) - (plr.x + dist)) + Math.PI;
                        new TurretBullet(x + 8, y + 12, dir * 180 / Math.PI, 4)
                        this.timer = 10;
                    }
                }
            }
        } else {
            this.segments['install'].draw(this.x, this.y)
            if (this.installed) {

                if (Math.round(this.segments['install'].frame.current) === 0) {
                    this.installed = false;
                    this.segments['install'].frame.speed = 0;
                }
            }
        }
    }

    castPlayer() {
        let {plr, map} = this.scene;
        let {x, y} = this;
        let dist = Fx.Math.Distance(plr.x, 0, x, 0) / 4 * plr.speed.x
        let dir = Math.atan2((y + 12) - plr.y, (x + 8) - plr.x) + Math.PI;

        for (let i = 0; i < 100; i++) {
            let xx = x + Math.cos(dir) * i * 4 + 8;
            let yy = y + Math.sin(dir) * i * 4+ 12
            if (map.isSolid(xx, yy)) {
                return true;
            }

            if (Fx.Math.Distance(plr.x, plr.y, xx, yy) < 8) {
                return false;
            }
        }

        return true;
    }

}