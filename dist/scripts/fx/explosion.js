class Explode extends Fx.Element {
    constructor(x, y) {
        super()
        this.properties = {
            explosion_count: 0
        }
        this.spriteIndex = SPRITES['explosion'];


        let scale = 1 + Math.random() * 2
        this.spriteIndex.scale = vec2(scale, scale);
        this.x = x;
        this.y = y;

        this.light = new PointLight();
        this.light.x = x;
        this.light.y = y;
        this.light.properties = {
            color: "#ffffff00",
            scale
        }
        if (this.x > this.scene.cameraX && this.x < this.scene.cameraX + Canvas.width && this.y > this.scene.cameraY && this.y < this.scene.cameraY + Canvas.height) {
            SOUNDS['exp'].play();
            Game.shake = scale * 4;
        } else {
            SOUNDS['exp_out'].play();
            Game.shake = scale * 2;
        }
        Game.Particles['explosion'].burst(x, y, 16);
    }

    update() {
        if (this.spriteIndex.frame.current == 6) {
            this.destroy();
            this.light.destroy();
        }
    }
}