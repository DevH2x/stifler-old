class TurretBullet extends Fx.Element {
    constructor(x, y, dir, speed){
        super();
        this.x = x;
        this.y = y;
        this.dir = dir;
        this._speed = speed;
    }

    update(){
        let {dir, _speed, x, y} = this;
        let {map} = this.scene;
        dir = dir / 180 * Math.PI
        this.speed.x = Math.cos(dir) * _speed;
        this.speed.y = Math.sin(dir) * _speed;
        Context
            .color("#f00")
            .fillRect(x - 1, y - 1, 2, 2);

        if(map.isSolid(x, y)){
            this.destroy()
        }
    }
}