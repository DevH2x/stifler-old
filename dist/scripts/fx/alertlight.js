class AlertLight extends Fx.Element{
    constructor(){
        super()
        this.sprite = Fx.ResourceManager.GetSprite('alert_light');
        this.sprite.scale = vec2(1,1)
        this.spawned = false;
    }
    update(){
        if(!this.spawned){
            this.x += 8;
            this.y += 8;
            this.spawned = true;
        }
        this.sprite.angle += 5;
        Context.globalCompositeOperation = "color-dodge";
        this.sprite.draw(this.x, this.y)
        Context.globalCompositeOperation = "source-over";
    }
}