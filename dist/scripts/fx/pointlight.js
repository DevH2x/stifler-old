class PointLight extends Fx.Element{
    constructor(){
        super()
        this.sprite = null;

        this.spawned = false;
        this.properties = {
            color: "#ffffffff",
            scale: 1
        }
        this.lightMap = null;
    }
    update(){
        if(!this.spawned){
            this.x += 8;
            this.y += 8;
            this.sprite = SPRITES['pointlight'];
            this.sprite.angle = this.properties.direction || 0;
            this.lightMap = Fx.Canvas.Create(128, 128,this.generateLight.bind(this));
            this.spawned = true;
        }

        if(this.lightMap) {
            Context.globalCompositeOperation = "color-dodge"
            Context.globalAlpha = this.properties.pulse? 0.7 + Math.random()*0.3 : 1;
            Context
                ._save()
                ._translate(this.x, this.y)
                ._scale(this.properties.scale,this.properties.scale)
                .drawImage(this.lightMap, - 64, - 64)
            Context.restore();
            Context.globalAlpha = 1;
            Context.globalCompositeOperation = "source-over"
        }
    }

    generateLight(ctx){
        let c = this.properties.color.split("#")[1].substr(2, 6);
        let a = this.properties.color.split("#")[1].substr(0, 2);
        ctx.fillStyle = "#000";
        ctx.fillRect(0,0,128, 128)
        ctx.globalAlpha = parseInt(a, 16) / 255;
        ctx.drawImage(this.sprite.source, 0, 0);
        ctx.globalAlpha = 1;
        ctx.globalCompositeOperation = "overlay";
        ctx.fillStyle = "#" + c;
        ctx.fillRect(0,0,128, 128)
        ctx.globalCompositeOperation = "source-over";
    }
}