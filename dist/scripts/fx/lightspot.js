class LightSpot extends Fx.Element{
    constructor(){
        super()
        this.sprite = null;

        this.spawned = false;
        this.properties = {
            color: "#ffffffff",
            angle: 0,
            long: false,
            broken: false
        }
        this.threshold = 8;
        this.lightMap = null;
    }
    update(){
        if(!this.spawned){
            this.x += 8;
            this.sprite = SPRITES[this.properties.long?'light_long':'light'];
            this.sprite.angle = this.properties.direction || 0;
            this.lightMap = Fx.Canvas.Create(64, this.properties.long?128:64,this.generateLight.bind(this));
            this.spawned = true;
        }

        if(this.lightMap && this.properties.broken? (Math.random() * 10 > this.threshold) : 1) {
            Context.globalCompositeOperation = "color-dodge"
            Context.globalAlpha = this.properties.pulse? 0.7 + Math.random()*0.3 : 1;
            Context
                ._save()
                ._translate(this.x, this.y)
                ._rotate(this.properties.direction / 180 * Math.PI)
                .drawImage(this.lightMap, -32, 0);
            Context.restore();
            Context.globalAlpha = 1;
            Context.globalCompositeOperation = "source-over"
        }
    }

    generateLight(ctx){
        ctx.fillStyle = "000";
        ctx.fillRect(0,0,64, this.properties.long?128:64)
        ctx.globalAlpha = 0.6
        ctx.drawImage(this.sprite.source, 0, 0);
        ctx.globalAlpha = 1;
        ctx.globalCompositeOperation = "overlay";
        ctx.fillStyle = "#" + this.properties.color.split("#")[1].substr(2, 6);
        ctx.fillRect(0,0,64, this.properties.long?128:64)
        ctx.globalCompositeOperation = "source-over";
    }
}