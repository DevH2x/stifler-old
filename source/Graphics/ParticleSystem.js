/*************************************/
/*
   particle system
   syntax:
   type = {
       life: [min, max],
       speed: [min, max],
       direction: [min, max],
       color: string hex,
       alpha: [0, 50%, 100%],
       scale: [min, max],
       gravity: float,
       blend: 'lighten'
   }
*/
/*************************************/


import Scene from "../Scene";

export default class ParticleSystem {
    constructor(type) {
        this.type = type;
        this.list = []
    }

    stream(x, y) {
        let {cameraX, cameraY} = Scene.Current;
        let {width, height} = Canvas;
        if(x > cameraX && x < cameraX + width && y > cameraY && y < cameraY + height)
            new Particle(x, y, this, this.type)
    }

    burst(x, y, c) {
        let {cameraX, cameraY} = Scene.Current;
        let {width, height} = Canvas;
        if(x > cameraX && x < cameraX + width && y > cameraY && y < cameraY + height) {
            for (let i = 0; i < c; i++)
                new Particle(x, y, this, this.type)
        }
    }

    update() {
        this.list.forEach(function (particle) {
            particle.x += Math.cos((particle.parameters.direction) / 180 * Math.PI) * (particle.parameters.speed);
            particle.y += Math.sin((particle.parameters.direction) / 180 * Math.PI) * (particle.parameters.speed) + particle.vspeed;
            particle.vspeed += particle.gravity || 0;
            particle.speed = particle.speed || 0;
            particle.direction = particle.direction || 0;
            particle.scale = particle.scale || 0;
            particle.parameters.speed += particle.speed[2] || 0;
            particle.parameters.direction += particle.direction[2] || 0;
            particle.parameters.scale += particle.scale[2] || 0;

            particle.drawshape(particle.shape);
            if (particle.step) {
                particle.step.stream(particle.x, particle.y)
            }

            if (particle.parameters.life-- < 1) {
                if (particle.outstream)
                    particle.outstream.stream(particle.x, particle.y)
                if (particle.outburst)
                    particle.outburst[0].burst(particle.x, particle.y, particle.outburst[1])
                this.destroyParticle(particle)
            }
        }.bind(this))
    }

    destroyParticle(p) {
        for (let i in this.list) {
            if (this.list[i] === p) {
                this.list.splice(i, 1);
                break;
            }
        }
    }

    setType(param) {
        this.type = param;
        return this;
    }
}

class Particle {
    constructor(x, y, parent, settings) {
        this.x = x;
        this.y = y;
        this.vspeed = 0;
        Object.assign(this, settings);
        parent.list.push(this);
        this.parameters = {
            life: this.life ? this.life[0] + Math.random() * (Math.abs(this.life[1]) - this.life[0]) : 30,
            speed: this.speed ? this.speed[0] + Math.random() * (Math.abs(this.speed[1]) - this.speed[0]) : 0,
            direction: this.direction ? this.direction[0] + Math.random() * (Math.abs(this.direction[1]) - this.direction[0]) : 0,
            angle: this.angle ? this.angle[0] + Math.random() * (Math.abs(this.angle[1]) - this.angle[0]) : 0,
            scale: this.scale ? this.scale[0] + Math.random() * (this.scale[0] - Math.abs(this.scale[1])) : 1
        };
        this.maxlife = this.parameters.life;
        this.angle = this.angle || 0;
        //this.alpha = Math.sin((this.parameters.life / this.maxlife) * Math.PI)
        if (typeof this.color === 'function')
            this.color = this.color();
    }

    drawshape(type) {
        type = type || 'none';
        switch (type) {
            case 'none':
                Context.globalCompositeOperation = this.blend || 'source-over';
                Context.globalAlpha =  Math.max(0, Math.min(Math.sin((this.parameters.life / this.maxlife) * Math.PI), this.alpha));
                Context
                    .color(this.color)
                    ._save()
                    ._translate(this.x, this.y)
                    ._scale(this.parameters.scale, this.parameters.scale)
                    ._fillRect(-2, -2, 4, 4)
                    ._restore();
                Context.globalAlpha = 1;
                Context.globalCompositeOperation = 'source-over';
                break;

            case 'circle':
                Context.globalCompositeOperation = this.blend || 'source-over';
                Context.globalAlpha = Math.max(0, Math.min(Math.sin((this.parameters.life / this.maxlife) * Math.PI), this.alpha));
                Context
                    .color(this.color)
                    ._save()
                    ._translate(this.x, this.y)
                    ._scale(this.parameters.scale, this.parameters.scale)
                    .fillCircle(0, 0, 4)
                    ._restore();
                Context.globalAlpha = 1;
                Context.globalCompositeOperation = 'source-over';
                break;

            case 'sprite':
                Context.globalCompositeOperation = this.blend || 'source-over';
                Context.globalAlpha =  Math.max(0, Math.min(Math.sin((this.parameters.life / this.maxlife) * Math.PI), this.alpha));
                Context
                    .color(this.color)
                    ._save()
                    ._translate(this.x, this.y)
                    ._scale(this.parameters.scale, this.parameters.scale)
                    .rotate(this.parameters.angle / 180 * Math.PI);
                    window.SPRITES[this.sprite].draw(0, 0);
                Context.restore();
                Context.globalAlpha = 1;
                Context.globalCompositeOperation = 'source-over';
                break;
            default:
                break;
        }
    }
}