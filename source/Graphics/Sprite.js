import {vec2} from "../Core/Functions";
import LoadController from '../Loader/LoadController';

export default class Sprite {
    constructor(options, cached_image) {
        this.ready = false;
        this.frame = {
            length: 0,
            size: options.frameSize || vec2(0, 0),
            current: 0,
            speed: options.speed === null? 0.5 : options.speed,
            row: 0
        };
        if(!cached_image) {
            LoadController.PlusTotal();
            this.source = new Image();
            this.source.src = options.file;
            this.source.crossOrigin = "Anonymous";
            let parent = this;
            this.source.onload = function () {
                parent.width = this.width;
                parent.height = this.height;
                parent.frame.length = Math.floor(this.width / parent.frame.size.x) || 0;
                parent.frame.length = (parent.frame.size.x === 0) ? 0 : parent.frame.length;
                parent.ready = true;
                LoadController.PlusLoaded();
            };
            this.options = options;
        }else{
            this.source = cached_image;
            this.width = this.source.width;
            this.height = this.source.height;
            this.frame.length = Math.floor(this.source.width / this.frame.size.x) || 0;
            this.frame.length = (this.frame.size.x === 0) ? 0 : this.frame.length;
            this.ready = true;
        }

        this.origin = options.origin || vec2(0, 0);
        this.width =
            this.height =
                this.angle = 0;
        this.scale = vec2(1, 1);

        this.speed = options.speed === null? 0.5 : options.speed;

        // for clone
        this.file = options.file;
        this.frameSize = this.frame.size;
    }

    draw(x, y) {
        let {Context} = window;
        x = Math.floor(x);
        y = Math.floor(y);
        if (this.ready) {
            if (this.frame.size.x === 0 || this.frame.size.y === 0) {
                Context.save();
                Context.translate(x, y);
                Context.scale(this.scale.x, this.scale.y);
                Context.rotate(this.angle / 180 * Math.PI);
                Context.drawImage(this.source, -this.origin.x, -this.origin.y);
                Context.restore();
            } else {
                this.frame.current += this.frame.speed;
                if (Math.floor(this.frame.current) > this.frame.length - 1)
                    this.frame.current = 0;
                if (Math.floor(this.frame.current) < 0)
                    this.frame.current = this.frame.length;
                Context.save();
                Context.translate(x, y);
                Context.scale(this.scale.x, this.scale.y);
                Context.rotate(this.angle / 180 * Math.PI);
                Context.drawImage(this.source,
                    Math.floor(this.frame.current) * this.frame.size.x, this.frame.row * this.frame.size.y,
                    this.frame.size.x, this.frame.size.y,
                    -this.origin.x, -this.origin.y,
                    this.frame.size.x, this.frame.size.y);
                Context.restore();
            }
        }
    }

    clone() {
        return new Sprite(this.options, this.source);
    }
}