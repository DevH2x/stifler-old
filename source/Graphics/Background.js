import {vec2} from "../Core/Functions";
import LoadController from '../Loader/LoadController';
import Scene from '../Scene';

export default class Background {
    constructor(options){

        let self = this;
        this.source = new Image();
        this.source.src = options.file;
        this.speed = options.speed || vec2(0,0);
        this.x = 0;
        this.y = 0;
        this.repeatX = (typeof options.repeatX === 'undefined')? true : options.repeatX;
        this.repeatY = (typeof options.repeatY === 'undefined')? true : options.repeatY;
        this.margin = options.margin || vec2(0,0);
        this.static = options.static || false;
        this.opacity = options.opacity === null?1:options.opacity;

        this.source.onload = function () {
            self.width = this.width;
            self.height = this.height;
            LoadController.PlusLoaded();
        };

        LoadController.PlusTotal();
    }

    update() {
        let {Context} = window;
        let s = Scene.Current;
        let w = (Canvas.width / this.width) * this.repeatX;
        let h = (Canvas.height / this.height) * this.repeatY;
        for(let i = -this.repeatX; i <= w; i++) {
            for(let j = -this.repeatY; j <= h; j++) {
                Context.globalAlpha = this.opacity;
                Context.drawImage(this.source, this.x + this.width * i + this.margin.x, this.y + this.height * j + this.margin.y);
                Context.globalAlpha = 1;
            }
        }


        if(this.static){
            this.x += this.speed.x;
            this.y += this.speed.y;
            if(this.x > this.width || this.x < -this.width)
                this.x = 0;
            if(this.y > this.height || this.y < -this.height)
                this.y = 0;
        }else{
            this.x = Math.ceil(s.cameraX / this.width) * this.width;
            this.y = Math.ceil(s.cameraY / this.height) * this.height;
        }
    }
}