import LoadController from "../Loader/LoadController";

let list = {};
let align = "lt";

export default class Font{
    static Load(options){
        LoadController.PlusTotal();
        let image = new Image();
        image.onload = function(){
            LoadController.PlusLoaded();
        };
        image.src = options.src;
        options.image = image;
        list[options.name] = options;
    }

    static DrawText(font, x, y, string, spaceminus){
        let f = list[font];
        let im = list[font].image;
        let columns = Math.round(im.width / f.letter.x);
        spaceminus = spaceminus || 0;
        let fullWidth = string.length * f.letter.x - string.length * spaceminus;
        let offsetX = align[0] === 'l'? 0 : align[0] === 'r' ? fullWidth : align[0] === 'c'? fullWidth / 2: 0;
        let offsetY = align[1] === 't'? 0 : align[1] === 'b' ? f.letter.y : align[1] === 'c'? f.letter.y / 2: 0;
        for(let i = 0; i < string.length; i++){
            let char = string[i].charCodeAt(0);
            if(!isNaN(string[i]) || string[i].match(/^[.,:!?]/))
                char -= 32;
            let charX = char % columns;
            let charY = Math.floor(char / columns);
            if(string[i] !== ' ') {

                Context.drawImage(im, charX * f.letter.x, charY * f.letter.y, f.letter.x, f.letter.y, x + f.letter.x * i - spaceminus * i - offsetX, y - offsetY, f.letter.x, f.letter.y)
            }
        }
    }

    static set Align(code){
        align = code;
    }

    static get List(){
        return list;
    }
}