import Core from '../Core';
import LoadController from "../Loader/LoadController";
import Input from "../Input";

let list = {
    blank: {
        load: function () {
            Fx.Canvas.BackgroundColor("#5af");
            let {Canvas, Context} = window;
            Context.wrapText = function (text, marginLeft, marginTop, maxWidth, lineHeight) {
                let words = text.split(" ");
                let countWords = words.length;
                let line = "";
                for (let n = 0; n < countWords; n++) {
                    let testLine = line + words[n] + " ";
                    let testWidth = Context.measureText(testLine).width;
                    if (testWidth > maxWidth) {
                        Context.fillText(line, marginLeft, marginTop);
                        line = words[n] + " ";
                        marginTop += lineHeight;
                    }
                    else {
                        line = testLine;
                    }
                }
                Context.fillText(line, marginLeft, marginTop);
            };
            let current = 0;
            let time = 0;
            let examples = [
                'Scene is not load',
                'Сцена не загружена',
                'Cцена не завантажена',
                'シーンがロードされていない',
                'Szene nicht geladen',
                '場景未加載'
            ];

            let animate = 0;
            Canvas.bgcolor = "#5af";

            let joke = "Oops... Sorry, but jokes not loaded";

            let xml = new XMLHttpRequest();
            xml.onreadystatechange = function (e) {
                if (e.target.responseText !== '') {
                    joke = JSON.parse(e.target.responseText).value.joke.replace(/&quot;/g, '"');
                }

            };
            xml.open('GET', 'http://api.icndb.com/jokes/random');
            xml.send();

            this.update = function () {
                let {Canvas, Context} = window;
                if (++time % 120 === 0)
                    current++;

                if (current > examples.length - 1)
                    current = 0;
                Context
                    ._font('24px sans-serif')
                    .align('cm')
                    .color('#fff')
                    ._fillText(examples[current],
                        Canvas.width / 2,
                        32)
                    ._fillText('Enjoy the joke <3',
                        Canvas.width / 2,
                        64)
                    ._font('14px sans-serif')
                    .wrapText(joke, Canvas.width / 2, Canvas.height / 2, 400, 25);
            }
        }
    },
    error: {
        load: function () {
            this.update = function () {
                Context
                    .color("#fff")
                    ._font('italic 16px sans-serif')
                    .align('lt')
                    ._fillText("Engine Error. Check console", 16, 16)
            }
        }
    }
};

let current = null;
let animate = 0;
let active = null;


export default class Scene {
    static Add(name, scn) {
        if (!scn || name === '') {
            Core.Error('Object is null or scene name are empty.')
        }
        list[name] = scn;
    }

    static get Current() {
        return active;
    }

    static Load(name) {
        if (!list[name]) {
            Core.Error('Scene is not found');
            return false;
        }
        if (!list[name].Objects)
            list[name].Objects = [];
        if(!list[name].NativeObjects)
            list[name].NativeObjects = [];
        if (!list[name].SolidElements)
            list[name].SolidElements = [];
        if (current && !list[current].inMemory) {
            if (list[current].Destroy) list[current].Destroy();
            list[current].__destroyObjects();
        }
        if (!list[name].__objectsUpdate) {
            list[name].__objectsUpdate = () => {
                list[name].Objects.forEach(e => {
                    e.loop();
                })
            }
        }
        if (!list[name].__destroyObjects) {
            list[name].__destroyObjects = () => {
                let {Objects, NativeObjects} = list[name];
                for (let i = 0; i < NativeObjects.length; i++) {
                    delete list[name][NativeObjects[i]];
                    NativeObjects.splice(i,1)
                }

                for (let i = 0; i < Objects.length; i++) {
                    Objects[i--].destroy();
                }

                console.log(Objects);
            }
        }
        active = list[name];
        current = name;
        if (!list[name].hasOwnProperty('cameraX')) list[name].cameraX = 0;
        if (!list[name].hasOwnProperty('cameraY')) list[name].cameraY = 0;
        if (!list[name].hasOwnProperty('rotate')) list[name].rotate = 0;
        if (!list[name].hasOwnProperty('zoom')) list[name].zoom = 1;
        if (!list[name].hasOwnProperty('cached')) list[name].cached = false;
        list[name].load();
        if(list[name].inMemory){
            list[name].cached = true;
        }
    }

    static AddNativeObject(name, object){
        Scene.Current[name] = object;
        Scene.Current.NativeObjects.push(name);
    }


    static Update() {
        let {Canvas, Context} = window;
        animate++;
        if (current === null)
            this.Load('blank');

        Context.save();
        Context.translate(Canvas.width / 2, Canvas.height / 2);

        Context
            ._save()
            ._translate(-Canvas.width / 2, -Canvas.height / 2)
            ._clearRect(0, 0, Canvas.width, Canvas.height)
            .restore();

        if (LoadController.Status.ready && current) {
            Input.GamepadUpdate();
            if(Fx.SceneBefore)
                Fx.SceneBefore();
            /**
             * Drawing BG
             */
            Context.save();
            Context.translate(-Canvas.width / 2, -Canvas.height / 2);
            Context
                .color(Canvas.bgc)
                ._fillRect(0, 0, Canvas.width, Canvas.height);
            if (active.bg)
                active.bg(animate);
            Context.restore();
            /**
             * Drawing SCENE
             */
            Context.save();
            Context.rotate(-active.rotate / 180 * Math.PI);
            Context.translate(-Canvas.width / 2 * active.zoom, -Canvas.height / 2 * active.zoom);
            Context.translate(-active.cameraX * active.zoom, -active.cameraY * active.zoom);
            Context.scale(active.zoom, active.zoom);

            if (active.update)
                active.update(animate);
            active.__objectsUpdate();
            Context.restore();
            /**
             * Draw GUI
             */
            Context.save();
            Context.translate(-Canvas.width / 2, -Canvas.height / 2)
            if (active.gui)
                active.gui(animate);
            Context.restore();

            if(Fx.SceneAfter)
                Fx.SceneAfter();
        }

        Context.restore();
        Input.mouseUpdate();
    }


    static Preloader() {
        let {Context, Canvas} = window;
        let color1 = "#5af";
        let color2 = "#eee";
        Context.save();
        Context.translate(Canvas.width / 2, Canvas.height / 2);
        Context.save();
        Context.translate(-Canvas.width / 2, -Canvas.height / 2);
        Context
            .color("#333")
            ._fillRect(0, 0, Canvas.width, Canvas.height);
        Context
            .align('cm')
            .color(color2)
            ._font('48px sans-serif')
            ._fillText("Foxal", Canvas.width / 2 - Context.measureText('JS').width / 2, Canvas.height / 2 - 32)
            ._font('bold 48px sans-serif')
            .color(color1)
            ._fillText("JS", Canvas.width / 2 + Context.measureText('Foxal').width / 2, Canvas.height / 2 - 32)
            .color("#222")
            ._fillRect(Canvas.width / 2 - Context.measureText("FoxalJS").width / 2, Canvas.height / 2, Context.measureText("FoxalJS").width, 3)
            .color(color1)
            ._fillRect(Canvas.width / 2 - Context.measureText("FoxalJS").width / 2, Canvas.height / 2, Context.measureText("FoxalJS").width * (LoadController.Status.loaded / LoadController.Status.total), 3)
            .restore();
        Context.restore();
    }
}
