import {GPads} from './index';
import Core from "../Core";
let Gamepad = {GKeys:[], GPads:{}};

Gamepad.GamepadConnecting = function (e, state) {
    let {Events} = window.Fx;
    let gamepad = window.navigator.getGamepads()[e.gamepad.index];
    if(state){
        if(Events.OnGamepadConnect)
            Events.OnGamepadConnect(e);
        window.Fx.Input.GPads[e.gamepad.index] = gamepad;
        Core.Log('Gamepad detected! ' + e.gamepad.id)
    }else{
        if(Events.OnGamepadDisconnect)
            Events.OnGamepadDisconnect(e);
        delete window.Fx.Input.GPads[e.gamepad.index];
    }
};

Gamepad.GamepadUpdate = function(){
    let gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : []);
    for(let i in gamepads){
        window.Fx.Input.GPads[i] = gamepads[i] || {buttons:null, axes:[]};
    }
};

Gamepad.GamepadCheckButton = function(id, num){
    if(window.Fx.Input.GPads[id].buttons){
        return window.Fx.Input.GPads[id].buttons[num].pressed;
    }
    return false;
}



export default Gamepad;