import Input from './';
import Scene from '../Scene';

let Mouse = {
    _mouseX: 0,
    _mouseY: 0,
    mouseX: 0,
    mouseY: 0,
    mouseStepX: 0,
    mouseStepY: 0,
};

Mouse.mouseDownHandler = function (e) {
    Mouse.mouseDown = true;
    Mouse.mousePressed = true;
};

Mouse.mouseUpHandler = function (e) {
    Mouse.mousePressed = false;
    Mouse.mouseUp = true;
};

Mouse.mouseReset = function () {
    Mouse.mouseDown =
        Mouse.mouseUp = false;
    this.mouseStepX =
        this.mouseStepY = 0;
};

Mouse.mouseUpdate = function () {
    Input.mouseX = Input._mouseX + Scene.Current.cameraX;
    Input.mouseY = Input._mouseY + Scene.Current.cameraY;
};

Mouse.mouseMoveHandler = function (e) {
    let {Canvas} = window;
    const cvs = Canvas.getBoundingClientRect();
    const scaleX = cvs.width / Canvas.width;
    const scaleY = cvs.height / Canvas.height;
    Input._mouseX = (e.clientX - cvs.left) / scaleX;
    Input._mouseY = (e.clientY - cvs.top) / scaleY;
    Input.mouseStepX = e.movementX;
    Input.mouseStepY = e.movementY;

};

export default Mouse;