import Input from './index';
let Keyboard =
    {
        Keys: []
    };
Keyboard.KeyDownHandler = function (e) {
    e = e || window.event;
    Input.Keys[e.keyCode] = true;

};
Keyboard.KeyUpHandler = function (e) {
    e = e || window.event;
    Input.Keys[e.keyCode] = false;
    if(e.keyCode === "P".charCodeAt(0)){
        window.Canvas.webkitRequestFullscreen();
    }
};
Keyboard.KeyCheck = function (code) {
    return Input.Keys[(typeof code === 'string') ? code.charCodeAt(0) : code] || false;
};

Keyboard.KeyReset = function (code) {
    Input.Keys[(typeof code === 'string') ? code.charCodeAt(0) : code] = false;
};


export default Keyboard;