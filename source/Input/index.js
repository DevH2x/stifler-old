import Keyboard from './Keyboard';
import Mouse from './Mouse';
import Gamepad from "./Gamepad";

let Input = {};
Input.initListeners = function()
{
    let {Canvas} = window;
    window.addEventListener('keydown', this.KeyDownHandler, false);
    window.addEventListener('keyup', this.KeyUpHandler, false);
    window.addEventListener('mousedown', this.mouseDownHandler, false);
    window.addEventListener('mouseup', this.mouseUpHandler, false);
    window.addEventListener('mousemove', this.mouseMoveHandler, false);
    window.addEventListener('gamepadconnected', e=>{this.GamepadConnecting(e, true)}, false);
    window.addEventListener('gamepaddisconnected', e=>{this.GamepadConnecting(e, false)}, false);
};
Object.assign(Input, Keyboard);
Object.assign(Input, Mouse);
Object.assign(Input, Gamepad);

export default Input;