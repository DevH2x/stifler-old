let vec2 = function(x, y){
    return {x, y};
};

let cloneObject = function(o){
    let c = {};
    for(let i in o){
        c[i] = o[i];
    }
    return c;
};


export {vec2, cloneObject}