import Scene from '../Scene';
import Loader from '../Loader';
import Input from "../Input";
import LoadController from "../Loader/LoadController";

export default class Core {
    static PrepareCanvas(id) {
        let {Config} = window.Fx;
        let Canvas = document.querySelector(id);
        let Context;
        if (!Config) {
            this.Error('Fx.Config is not found. ');
            return false;
        }
        if (!Canvas) {
            this.Error('Canvas ' + id + ' is not found. ');
            return false;
        }
        Object.assign(Canvas, Config);
        Context = Canvas.getContext('2d');
        Canvas.style.backgroundColor = Canvas.bgColor || "#fff";
        Canvas.style.imageRendering = Config.imageSmoothingEnabled ? 'auto' : 'pixelated';
        Context.imageSmoothingEnabled = Config.imageSmoothingEnabled;
        Object.assign(window, {Canvas, Context});
        Core.PrepareContext();
        Input.initListeners();
        Loader.LoadResources();
        this.Render();
    }

    static PrepareContext() {
        let Context = {};

        Context._save = function () {
            window.Context.save();
            return window.Context;
        };

        Context._restore = function () {
            window.Context.restore();
            return window.Context;
        };

        Context._translate = function (x, y) {
            window.Context.translate(x, y);
            return window.Context;
        };

        Context._scale = function (x, y) {
            window.Context.scale(x, y);
            return window.Context;
        };

        Context._rotate = function (r) {
            window.Context.rotate(r);
            return window.Context;
        };

        Context.align = function (code) {
            window.Context.textBaseline =
                (code[1] === 'm') ? "middle" :
                    (code[1] === 't') ? "top" :
                        (code[1] === 'b') ? "bottom" : 'top';
            window.Context.textAlign =
                (code[0] === 'c') ? "center" :
                    (code[0] === 'l') ? "left" :
                        (code[0] === 'r') ? "right" : 'left';

            return window.Context;
        };

        Context._transform = function (a, b, c, d, e, f) {
            window.Context.drawImage(a, b, c, d, e, f);
            return window.Context;
        };

        Context._fillRect = function (x, y, w, h) {
            window.Context.fillRect(x, y, w, h);
            return window.Context;
        };

        Context._clearRect = function (x, y, w, h) {
            window.Context.clearRect(x, y, w, h);
            return window.Context;
        };

        Context._drawImage = function (a, b, c, d, e, f, g, h, i) {
            window.Context.drawImage(a, b, c, d, e, f, g, h, i);
            return window.Context;
        };

        Context._fillText = function (t, x, y) {

            window.Context.fillText(t, x, y)
            return window.Context;
        };
        Context.color = function (c) {

            window.Context.fillStyle = window.Context.strokeStyle = c;
            return window.Context;
        };
        Context._font = function (f) {

            window.Context.font = f;
            return window.Context;
        };

        Context.fillCircle = function (x, y, r) {
            window.Context.beginPath();
            window.Context.arc(x, y, r, 0, Math.PI * 2);
            window.Context.fill();
        };

        Object.assign(window.Context, Context);
    }

    static Error(str) {
        console.error('[FoxalJS Error]: ' + str);
    }

    static Warn(str) {
        console.warn('[FoxalJS Warning]: ' + str);
    }

    static Log(str) {
        console.log('[FoxalJS Log]: ' + str);
    }

    static Render() {
        if (LoadController.Status.ready) {
            Scene.Update();
        }else{
            Scene.Preloader();
        }
        window.requestAnimationFrame(Core.Render);
    }
};