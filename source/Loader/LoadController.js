let loaded = 0,
    total = 0,
    ready = false;

export default class LoadController {
    static PlusTotal(){
        total++;
        if(loaded !== total) {
            ready = false;
        }
    }

    static PlusLoaded(){
        loaded++;
        if(loaded === total) {
            ready = true;
            if(window.Fx.OnLoad)
                window.Fx.OnLoad();
            window.Fx.OnLoad = null;
        }
    }

    static get Status(){
        return {loaded, total, ready};
    }

    static set Ready(bool){
        ready = bool;
    }
}