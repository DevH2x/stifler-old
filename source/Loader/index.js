import LoadController from './LoadController';
import Core from '../Core';
import Sprite from "../Graphics/Sprite";
import Background from "../Graphics/Background";
import Map from "../Map";
import Font from "../Graphics/Fonts";

export default class ResourceManager {
    static LoadResources() {
        if (!window.Fx.Resources) {
            Core.Warn('Resource Storage is not defined');
            return false;
        }
        ResourceManager.LoadScripts();
        ResourceManager.LoadSprites();
        ResourceManager.LoadBackgrounds();
        ResourceManager.LoadMaps();
        if (window.Howl) {
            console.log('Howler Detected!')
            ResourceManager.LoadSounds();
        }
        ResourceManager.LoadFonts();
    }

    static LoadScripts(i) {
        i = i || 0;
        let {Scripts} = window.Fx.Resources;
        if (!Scripts)
            return;
        if (!Scripts[i]) return;
        let e = document.createElement('script');
        e.src = Scripts[i];
        e.defer = true;
        e.onload = function () {
            window.Fx.ResourceManager.LoadScripts(i + 1);
            LoadController.PlusLoaded();
            Core.Log(Scripts[i]);
        };

        document.body.appendChild(e);
        LoadController.PlusTotal();
    }

    static LoadSprites() {
        let {Sprites} = window.Fx.Resources;
        if (!Sprites)
            return;
        for (let k in Sprites) {
            let options = Sprites[k];
            Sprites[k] = new Sprite(options);
        }
        window['SPRITES'] = Sprites;
    }

    static LoadBackgrounds() {
        let {Backgrounds} = window.Fx.Resources;
        if (!Backgrounds)
            return;
        for (let k in Backgrounds) {
            let options = Backgrounds[k];
            Backgrounds[k] = new Background(options);
        }
        window['BACKGROUNDS'] = Backgrounds;
    }

    static LoadSounds() {
        let {Sounds} = window.Fx.Resources;
        if (!Sounds)
            return;
        for (let k in Sounds) {
            let options = Sounds[k];
            LoadController.PlusTotal();
            Sounds[k] = new Howl(options);
            Sounds[k].once('load', function () {
                LoadController.PlusLoaded();
            })
        }
        window['SOUNDS'] = Sounds;
    }

    static LoadMaps() {
        let {Maps} = window.Fx.Resources;
        if (!Maps)
            return;
        for (let k in Maps) {
            LoadController.PlusTotal();
            Map.Load(Maps[k].file, function (map) {
                LoadController.PlusLoaded();
                if (Maps[k].hasOwnProperty('solid'))
                    map.setSolid(Maps[k].solid);
                Maps[k] = map;
                window['MAPS'] = Maps;
            });
        }

    }

    static LoadFonts() {
        let {Fonts} = window.Fx.Resources;
        if (!Fonts)
            return;
        for (let k in Fonts) {
            Fonts[k].name = k;
            Font.Load(Fonts[k]);
        }
        window['FONTS'] = Fonts;
    }

    static GetSprite(name) {
        if (window['SPRITES'] && window['SPRITES'][name])
            return window['SPRITES'][name].clone();
        else
            return Core.Error('Sprite "' + name + '" is not defined');
    }
}