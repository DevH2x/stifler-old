import Scene from "../Scene";

export default class ObjectController {

    static AddSolidElement(element){
        Scene.Current.SolidElements.push(element);
    }

    static Swap(obj1, obj2){
        let id1 = obj1.id;
        let id2 = obj2.id;


        let temp = Scene.Current.Objects[id1];
        Scene.Current.Objects[id1] = Scene.Current.Objects[id2];
        Scene.Current.Objects[id2] = temp;

        obj1.id = id2;
        obj2.id = id1;
    }
}