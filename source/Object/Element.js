import Scene from '../Scene';
import Core from "../Core";
import ObjectController from "./Controller";

export default class Element {
    constructor() {
        this.x = 0;
        this.y = 0;
        this.speed = vec2(0, 0);
        this.list = Scene.Current.Objects;
        this.scene = Scene.Current;
        this.rect = vec2(32, 32);
        this.origin = vec2(0, 0);
        this.properties = {};
        this.list.push(this);
        this.spriteIndex = null;
        this.id = this.list.length - 1;
    }

    loop() {
        this.__updateid();
        if (this.draw)
            this.draw();
        if (this.update)
            this.update();

        if (this.spriteIndex !== null) {
            if (typeof this.spriteIndex !== 'object')
                throw Core.Error("Attempt to draw a non-existing sprite");
            this.spriteIndex.draw(this.x, this.y);
        }
        this.assign();
    }

    assign() {
        this.x += this.speed.x;
        this.y += this.speed.y;
    }

    destroy() {
        let {Objects} = Scene.Current;
        let found = false;
        for (let i in Objects) {
            if (Objects[i] === this) {
                Objects.splice(i, 1);
                break;
            }
        }


        for (let i = 0; i < this.scene.SolidElements.length; i++) {
            if (this.scene.SolidElements[i] === this) {
                this.scene.SolidElements.splice(i, 1);
                break;
            }
        }
    }

    __updateid() {
        for (let i in Scene.Current.Objects) {
            if (Scene.Current.Objects[i] === this) {
                this.id = i;
                break;
            }
        }
    }

    static get name() {
        return 'ObjectElement';
    }

    insolid(x, y) {
        for (let i in this.scene.SolidElements) {
            let o = this.scene.SolidElements[i];
            if (this.x + x >= o.x && this.x + x <= o.x + o.rect.x &&
                this.y + y >= o.y && this.y + y <= o.y + o.rect.y) {
                return o;
            }
        }

        return false;
    }

    incamera(){
       return this.x > this.scene.cameraX && this.x < this.scene.cameraX + Canvas.width && this.y > this.scene.cameraY && this.y < this.scene.cameraY + Canvas.height
    }
}