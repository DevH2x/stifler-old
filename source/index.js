import Core from './Core';
import Canvas from './Canvas';
import Scene from './Scene';
import LoadController from './Loader/LoadController';
import Input from './Input';
import Map from './Map';
import Element from './Object/Element';
import ResourceManager from './Loader/index';
import {vec2} from "./Core/Functions";
import ObjectController from "./Object/Controller";
import Math from "./Math";
import Font from "./Graphics/Fonts";
import ParticleSystem from "./Graphics/ParticleSystem";

Object.assign(window, {
    vec2
});
window.Fx = {Core, Canvas, Scene, LoadController, Input, Map, Element, ResourceManager, Events: {}, ObjectController, Math, Font, ParticleSystem};


