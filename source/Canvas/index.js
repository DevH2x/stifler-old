export default class Canvas {
    static Fit(w, h){
        Object.assign(window.Canvas.style, {
            width: w + 'px',
            height: h + 'px'
        })
    }

    static Size(w, h){
        Object.assign(window.Canvas, {
            width: w,
            height: h
        })
    }

    static BackgroundColor(hex){
        window.Canvas.style.backgroundColor =
        window.Canvas.bgc = hex;
    }

    static Create(w, h, draw, alpha){
        let canvas = document.createElement('canvas');
        alpha = alpha || true;
        canvas.width = w;
        canvas.height = h;
        draw(canvas.getContext('2d', {alpha}));
        //document.body.appendChild(canvas);
        // console.log(canvas);
        return canvas;
    }
}