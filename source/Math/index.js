export default class Math {
    static Distance(x, y, x2, y2){
        return window.Math.sqrt((x - x2) ** 2 + (y - y2) ** 2);
    }
}