import LoadController from '../Loader/LoadController';
import Scene from "../Scene";
import Canvas from "../Canvas";
import {cloneObject} from "../Core/Functions";

let Tilesets = {};
let Maps = {};


export default class Map {

    static Load(json, callback) {
        let name = json.split('/');
        name = name[name.length - 1].split('.')[0];
        fetch(json)
            .then(response => {
                response.json()
                    .then(value => {

                        Maps[name] = {solid: []};
                        Object.assign(Maps[name], value);
                        Maps[name].tileset = Map.LoadTileset(value.tilesets[0]);

                        Maps[name].display = function () {
                            let canv = window.Canvas;
                            let {tileset, layers, width, height} = this;
                            let {Current} = Scene;
                            for (let layer = 0; layer < layers.length; layer++) {
                                let l = layers[layer];
                                if (l.type === 'objectgroup') continue;
                                if (l.name[0] === '$') continue;
                                for (let j = 0; j < height; j++) {
                                    let y = j * tileset.tileheight;
                                    if (y < Current.cameraY - tileset.tileheight * 2 || y > Current.cameraY + canv.height) continue;
                                    for (let i = 0; i < width; i++) {
                                        let index = j * width + i;
                                        let x = i * tileset.tilewidth;
                                        if (x < Current.cameraX - tileset.tilewidth * 2 || x > Current.cameraX + canv.width) continue;
                                        if (l.data[index]) {
                                            tileset.draw(l.data[index], i, j);
                                        }
                                    }
                                }
                            }
                        };

                        Maps[name].setSolid = function (layername) {
                            let {layers} = this;
                            for (let i = 0; i < layers.length; i++) {
                                if (layers[i].name === layername) {
                                    this.solid = layers[i].data;
                                }
                            }
                        };

                        Maps[name].isSolid = function (x, y) {
                            x = Math.floor(x / value.tilewidth);
                            y = Math.floor(y / value.tileheight);
                            return this.solid[y * value.width + x] || false;
                        };

                        Maps[name].prepareObjects = function () {
                            Canvas.BackgroundColor(value.backgroundcolor || "#fff");
                            let {Current} = Scene;
                            for (let i = 0; i < value.layers.length; i++) {
                                let layer = value.layers[i];
                                if (layer.type === 'objectgroup') {
                                    for (let j = 0; j < layer.objects.length; j++) {
                                        let e = layer.objects[j];
                                        let o = eval('new ' + e.type);
                                        let newProp = Object.assign(o.properties, cloneObject(e.properties))
                                        Object.assign(o, e);
                                        o.properties = newProp;
                                        if(e.type == 'elevator')
                                            console.log(o.properties)
                                        o.y -= value.tileheight;
                                        if(e.name){
                                            Scene.AddNativeObject(e.name, o);
                                        }
                                    }

                                    Current.Objects.forEach(e=>{
                                        if(e.onSpawn)
                                            e.onSpawn();
                                    })
                                }
                            }
                        };



                        if (typeof callback === 'function') {
                            callback(Maps[name]);
                        }

                    })
            });
        return name;
    }

    static LoadTileset(data) {
        let {Context} = window;
        LoadController.PlusTotal();
        Tilesets[data.name] = {
            file: new Image(),
            draw: function (index, x, y) {
                let {file, tilewidth, tileheight, columns} = this;
                //columns--;
                index--;
                let ty = Math.floor(index / columns);
                let tx = index % columns;
                Context.drawImage(file, tx * tilewidth, ty * tileheight, tilewidth, tileheight, x * tilewidth, y * tileheight, tilewidth, tileheight);
            }
        };
        Object.assign(Tilesets[data.name], data);
        Tilesets[data.name].file.src = data.image;
        Tilesets[data.name].file.onload = function () {
            LoadController.PlusLoaded();
        };

        return Tilesets[data.name];
    }

    static get Tilesets() {
        return Tilesets;
    }

    static get List() {
        return Maps;
    }
}